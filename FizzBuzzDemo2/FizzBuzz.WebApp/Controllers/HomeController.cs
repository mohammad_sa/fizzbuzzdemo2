﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FizzBuzz.WebApp.Models;
using FizzBuzz.Services;
// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace FizzBuzz.WebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly ITextGenerator _messages;
        private readonly IDayOfTheWeek _dayOfTheWeek;
        public HomeController(ITextGenerator messages, IDayOfTheWeek dayOfTheWeek)
        {
            _messages = messages;
            _dayOfTheWeek = dayOfTheWeek;
        }
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Index(IndexModel indexModel, int currentPageIndex = 1)
        {
            if (ModelState.IsValid)
            {
                int maxRows = 20;
                IList<string> lstMessages  = _messages.GetMessages((int)indexModel.InputData, _dayOfTheWeek.GetCurrentDay().ToString());

                IndexModel result = new IndexModel
                {
                    Messages = lstMessages.Skip((currentPageIndex - 1) * maxRows).Take(maxRows).ToList(),
                    PageCount = (int)Math.Ceiling((double)(lstMessages.Count() / Convert.ToDecimal(maxRows))),
                    CurrentPageIndex = currentPageIndex,
                    InputData = indexModel.InputData
                };

                return View(result);
            }
            else
            {
                return View();
            }
        }

        
    }
}

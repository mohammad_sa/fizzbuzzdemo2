using FizzBuzz.WebApp.Controllers;
using FizzBuzz.WebApp.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using Xunit;

namespace FizzBuzz.WebApp.Test
{
    public class HomeControllerTest
    {
        [Fact]
        public void Test_Index_ReturnView()
        {
            //Arrange
            var result = new HomeController();

            //Act
            var actualResult = result.Index();

            //Assert
            Assert.IsAssignableFrom<IActionResult>(actualResult);
        }

        [Theory]
        [InlineData(30)]
        public void Test_Index_PostMethod_ReturnsView(int inputData)
        {
            //Arrange
            var result = new HomeController();

            //Act
            var actualResult = result.Index(inputData);

            //Assert
            Assert.IsAssignableFrom<IActionResult>(actualResult);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FizzBuzz.Services
{
    public interface ITextGenerator
    {
        public IList<string> GetMessages(int inputNumber, string dayOfTheWeek);
    }
}

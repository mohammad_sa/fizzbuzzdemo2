﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FizzBuzz.Services.Services.TextGenerator
{
    public class ApplicationTextGenerator : ITextGenerator
    {
        private readonly IEnumerable<IRule> _Rules;

        public ApplicationTextGenerator(IEnumerable<IRule> Rules)
        {
            _Rules = Rules;
        }
        public IList<string> GetMessages(int inputNumber, string dayOftheWeek)
        {
            IList<string> messages = new List<string>();

            for (int sequenceNumber = 1; sequenceNumber <= inputNumber; sequenceNumber++)
            {
                messages.Add(ProcessFizzBuzzData(sequenceNumber, dayOftheWeek));
            }
            return messages;
        }

        public string ProcessFizzBuzzData(int sequenceNumber, string dayOftheWeek)
        {
            foreach (var rule in _Rules)
            {
                if (rule.IsNumberMatched(sequenceNumber))
                {
                    return rule.GetReplacedWord(dayOftheWeek);
                }
            }
            return Convert.ToString(sequenceNumber);
        }

    }
}

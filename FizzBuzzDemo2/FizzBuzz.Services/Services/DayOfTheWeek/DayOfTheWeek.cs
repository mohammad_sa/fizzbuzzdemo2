﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FizzBuzz.Services.Services.DayOfTheWeek
{
    public class DayOfTheWeek : IDayOfTheWeek
    {
        public DayOfWeek GetCurrentDay()
        {
            return DateTime.Today.DayOfWeek;
        }
    }
}

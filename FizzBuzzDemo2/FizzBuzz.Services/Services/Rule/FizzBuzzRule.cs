﻿using FizzBuzz.Services.Constant;
using System;
using System.Collections.Generic;
using System.Text;

namespace FizzBuzz.Services.Services.Rule
{
    public class FizzBuzzRule : IRule
    {
        public bool IsNumberMatched(int enteredNumber)

        {
            return enteredNumber % 3 == 0 && enteredNumber % 5 == 0;
        }

        public string GetReplacedWord(string dayOftheWeek)
        {
            return (dayOftheWeek == Constants.DayWeekRule) ? "Wizz Wuzz" : "Fizz Buzz";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FizzBuzz.Services
{
    public interface IRule
    {
        bool IsNumberMatched(int inputNumber);
        string GetReplacedWord(string dayOftheWeek);
    }
}

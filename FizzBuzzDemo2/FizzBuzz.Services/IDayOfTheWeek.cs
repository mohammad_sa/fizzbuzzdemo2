﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FizzBuzz.Services
{
    public interface IDayOfTheWeek
    {
        DayOfWeek GetCurrentDay();
    }
}
